package yakiv.bondar.dev.intershiptask1.ui.activities;

import android.content.Context;

/**
 * This class is a presenter for the MainActivity.
 * Implements methods for requesting urls from string resources,
 * onclick listener for MainActivity views and home button navigation.
 */
public class MainPresenterImpl implements MainPresenter, MainInteractor.OnDeliveredUrlListener {

    private MainView mMainView;
    private MainInteractor mMainInteractor;

    public MainPresenterImpl(MainView mainView) {
        mMainView = mainView;
        mMainInteractor = new MainInteractorImpl();
    }

    @Override
    public void requestUrls(Context context) {
        mMainInteractor.getUrls(this, context);
    }

    @Override
    public void onDeliveredUrl(String[] urls) {
        mMainView.setmUrls(urls);
    }

    @Override
    public void onDestroy() {
        mMainView = null;
    }
}
