package yakiv.bondar.dev.intershiptask1.ui.activities;

import android.content.Context;

import yakiv.bondar.dev.intershiptask1.R;

/**
 * This class implements methods for getting string resources.
 */
public class MainInteractorImpl implements MainInteractor {

    @Override
    public void getUrls(OnDeliveredUrlListener listener, Context context) {
        String [] str = getImageLinks(context);
        listener.onDeliveredUrl(str);
    }

    private String[] getImageLinks(Context context) {
        String [] urls = {
                context.getString(R.string.mai_url1),
                context.getString(R.string.mai_url2),
                context.getString(R.string.mai_url3),
        };
          return urls;
    }
}
