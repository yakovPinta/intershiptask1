package yakiv.bondar.dev.intershiptask1.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import yakiv.bondar.dev.intershiptask1.R;

/**
 * Is responsible for retrieving data for RecyclerView.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    public static final int IMAGE_SIDE = 300;
    private String[] mUrls;
    private Context mContext;

    public DataAdapter(String[] urls, Context context) {
        mUrls = urls;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_container, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(mContext)
                .load(mUrls[position])
                .resize(IMAGE_SIDE, IMAGE_SIDE)
                .into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return mUrls.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv)
        ImageView mImageView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
