package yakiv.bondar.dev.intershiptask1.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import yakiv.bondar.dev.intershiptask1.R;
import yakiv.bondar.dev.intershiptask1.adapters.DataAdapter;
import yakiv.bondar.dev.intershiptask1.utils.ItemOffsetDecoration;

/**
 * Main activity class.
 */
public class MainActivity extends AppCompatActivity implements MainView {

    private MainPresenter mMainPresenter;

    private String [] mUrls;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setPresenter();
        initUrls();
        initViews();
    }

    private void setPresenter() {
        mMainPresenter = new MainPresenterImpl(this);
    }

    private void initViews() {
        initToolbar();

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        initLinearLayoutManager(recyclerView);
        recyclerView.addItemDecoration(new ItemOffsetDecoration((int) getResources().
                getDimension(R.dimen.rv_item_offset)));
        setAdapter(recyclerView);
    }

    private void setAdapter(RecyclerView recyclerView) {
        DataAdapter adapter = new DataAdapter(mUrls, getApplicationContext());
        recyclerView.setAdapter(adapter);
    }

    private void initLinearLayoutManager(RecyclerView recyclerView) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    private void initToolbar() {
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void onClickViews(View view){
        Toast.makeText(getApplicationContext(), view.getClass().getSimpleName(), Toast.LENGTH_SHORT).show();
    }

    //Initialize mUrls variable from string resources.
    @Override
    public void initUrls()
    {
        mMainPresenter.requestUrls(getApplicationContext());
    }

    public void setmUrls(String[] mUrls) {
        this.mUrls = mUrls;
    }

    //Set homeAsUp navigation.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override protected void onDestroy() {
        mMainPresenter.onDestroy();
        super.onDestroy();
    }
}