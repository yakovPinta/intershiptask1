package yakiv.bondar.dev.intershiptask1.ui.activities;

/**
 * Declares methods for MainActivity.
 */
public interface MainView {
    void initUrls();

    void setmUrls(String [] urls);
}
