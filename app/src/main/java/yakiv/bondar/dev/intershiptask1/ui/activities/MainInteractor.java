package yakiv.bondar.dev.intershiptask1.ui.activities;

import android.content.Context;

/**
 * Is a contract for presenter.
 */
public interface MainInteractor {
    interface OnDeliveredUrlListener{
        void onDeliveredUrl(String [] urls);
    }

    void getUrls(OnDeliveredUrlListener listener, Context context);
}
