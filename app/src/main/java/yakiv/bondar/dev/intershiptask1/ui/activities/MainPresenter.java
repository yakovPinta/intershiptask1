package yakiv.bondar.dev.intershiptask1.ui.activities;

import android.content.Context;

/**
 * Declares methods for presenter.
 */
public interface MainPresenter {
    void requestUrls(Context context);

    void onDestroy();
}
